﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class Markers
{
    public List<Sprite> image;
    public string markerName;
}
