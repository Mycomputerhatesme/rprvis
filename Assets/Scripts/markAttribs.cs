﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class markAttribs : MonoBehaviour 
{
	public GameObject OtherPlayer;
	public int playerNumber = 0;

	public GameObject ctrlObj;
	private visCtrl visMain;
	public ZoomMapAttribs zoomMarker;
	public ZoomMapAttribs zoomMarkerOther;

	public Vector3 otherPlayerLocation;
	public float distanceToOtherPlayer;
	public float time;
	public int test;

	public bool weCool;

	public string annEvent, annNature, dialogue;

	// Use this for initialization

	private void Awake()
	{
		
	}

	void Start() 
	{
		if(playerNumber == 1)
		{
			OtherPlayer = Instantiate(this.gameObject, this.transform.parent);
			otherPlayerLocation = new Vector3(Random.Range(-250.0f, 250.0f), Random.Range(-200.0f, 200.0f), 0.0f);
			OtherPlayer.transform.localPosition = otherPlayerLocation;
			OtherPlayer.GetComponent<markAttribs>().playerNumber = 2;
			OtherPlayer.GetComponent<markAttribs>().OtherPlayer = this.gameObject;
		}
		else
		{

		}

		int imageSelectColor = 0;
		if(this.gameObject.tag == "Checkpoint")
			imageSelectColor = 0;
		else if(this.gameObject.tag == "Death")
			imageSelectColor = 1;
		else if(this.gameObject.tag == "Comm")
			imageSelectColor = 2;

		int imageSelectPlayer = playerNumber - 1;
		GetComponent<Image>().sprite = GameObject.Find("ZoomMap").GetComponent<ZoomMap>()._iconImages[imageSelectPlayer].image[imageSelectColor];
		visMain = ctrlObj.GetComponent<visCtrl>();

		distanceToOtherPlayer = Vector3.Distance(this.transform.localPosition, OtherPlayer.transform.localPosition);
	}
	
	// Update is called once per frame
	void Update() 
	{
		GetComponent<Image>().enabled = false;
		if(zoomMarker != null)
		{
			GetComponentInChildren<LineRenderer>().enabled = zoomMarker.lineActive;
			zoomMarker.GetComponentInChildren<LineRenderer>().enabled = zoomMarker.lineActive;
		}
		else
			Debug.LogWarning("zoomMarker is null");
		//Checks if mark is in the proper test
		if (visMain.testNum == test || visMain.testNum == 0) 
		{
			if (this.gameObject.tag == "Checkpoint" && visMain.isChk) 
			{
				//now test for time
				if (time >= visMain.minTime && time <= (5-visMain.maxTime)) 
				{
					//Now we can be visible
					GetComponent<Image>().enabled = true;
					if(visMain.forceLine && zoomMarker != null)
					{
						GetComponentInChildren<LineRenderer>().enabled = true;
						zoomMarker.GetComponentInChildren<LineRenderer>().enabled = true;
					}
				}
			} 
			else if (this.gameObject.tag == "Death" && visMain.isDeath) 
			{
				//now test for time
				if (time >= visMain.minTime && time <= (5-visMain.maxTime)) 
				{
					//Now we can be visible
					GetComponent<Image>().enabled = true;
					if(visMain.forceLine && zoomMarker != null)
					{
						GetComponentInChildren<LineRenderer>().enabled = true;
						zoomMarker.GetComponentInChildren<LineRenderer>().enabled = true;
					}
				}
			} 
			else if (this.gameObject.tag == "Comm" && visMain.isComm) 
			{
				//now test for time
				if (time >= visMain.minTime && time <= (5-visMain.maxTime)) 
				{
					//Now we can be visible
					GetComponent<Image>().enabled = true;
					if(visMain.forceLine && zoomMarker != null)
					{
						GetComponentInChildren<LineRenderer>().enabled = true;
						zoomMarker.GetComponentInChildren<LineRenderer>().enabled = true;
					}
				}
			}
		}
	}
}
