﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomMapAttribs : MonoBehaviour 
{
	public GameObject linePrefab;
	GameObject lineObjectMini;
	LineRenderer lineMini;
	GameObject lineObject;
	LineRenderer line;
	public markAttribs _markAttribs;
	visCtrl visMain;
	MiniMap _miniMap;
	ZoomMap _zoomMap;
	public Vector3 otherPlayerLocation;
	public Vector3 inverseLerpResult;
	public bool lineActive;
	Vector3 _scale;

	// Use this for initialization
	void Start () 
	{
        visMain = GameObject.Find("MainCtrl").GetComponent<visCtrl>();
		_miniMap = GameObject.Find("MiniMap_Img").GetComponent<MiniMap>(); 

		_markAttribs.zoomMarker = this;
		_zoomMap =_miniMap.zoomMap;
		_scale.x = _zoomMap.GetComponent<RectTransform>().rect.width / _miniMap.GetComponent<RectTransform>().rect.width;
		_scale.y = _zoomMap.GetComponent<RectTransform>().rect.height / _miniMap.GetComponent<RectTransform>().rect.height;
		_scale.x *= 1.2365f;
		//_markAttribs.

		lineObject = Instantiate(linePrefab, transform);
		lineObjectMini = Instantiate(linePrefab, _markAttribs.transform);
		
		lineObject.transform.Translate(0f,0f,-25f);
		lineObjectMini.transform.Translate(0f,0f,-15f);
		
		line = lineObject.GetComponent<LineRenderer>();
		lineMini = lineObjectMini.GetComponent<LineRenderer>();
		
		
	}

	public void AnnoOpen()
	{
		lineActive = true;
		
		if (true)//(_markAttribs.tag == "Comm")
		{
			Vector3 offset = transform.position - _markAttribs.transform.position + new Vector3(-0f, 0f);
		    visMain.AnnotationOpen(_markAttribs.gameObject, offset);
		}
	}
	
	public void AnnoClose()
	{
		lineActive = false;

		if(true)//(_markAttribs.tag == "Comm")
			visMain.AnnotationClose();
	}

	// Update is called once per frame
	void Update () 
	{
		Vector3 scl = (_markAttribs.OtherPlayer.transform.localPosition -  _markAttribs.transform.localPosition) / _miniMap.zoomSize;
		scl.Scale(_scale);
		line.SetPosition(1, scl);
		lineMini.SetPosition(1, (_markAttribs.OtherPlayer.transform.localPosition -  _markAttribs.transform.localPosition));


	}
}
