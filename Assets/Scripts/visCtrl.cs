﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class visCtrl : MonoBehaviour 
{

	public int testNum;
	public Dropdown test;

	//For the double slider positions/time filter
	public float minTime;
	public float maxTime;
	public GameObject sliderObjMin;
	public GameObject sliderObjMax;

	//For the single slider:
	public GameObject singleSliderObj;
	public float singleSliderVal;

	//For the other filter options
	public bool isAnn;
	public bool isChk;
	public bool isDeath;
	public bool isComm;
	public bool forceLine;
	public Toggle ann;
	public Toggle chk;
	public Toggle death;
	public Toggle comm;
	public Toggle isDist;

	//For swapping b/w the range slider and pt slider
	public Toggle rangeSlider;
	public Toggle pointSlider;
	//The 'greying out' of what is not needed.
	//public Image distBlocker;
	public Image rangeBlocker;
	public Image pointBlocker;

	//To make up for the +/- range of changing slider values (i.e. 1.1 = 1)
	public Text errorInput;
	public float SLIDER_ERROR;

	//Holds the Annotation box
	public GameObject AnnBox;

	//The x and y offsets of the annotation boxes.
	const float annX = -200.0f, annY = 40.0f;

	// Use this for initialization
	void Start() 
	{

	}

	//Closes out of the program
	public void CloseMe(){
		Application.Quit ();
	}

	//Opens the Annotation box (with correct text) next to selected Comm point
	public void AnnotationOpen(GameObject dot)
	{
		AnnotationOpen(dot, Vector3.zero);
	}

	//Opens the Annotation box (with correct text) next to selected Comm point
	public void AnnotationOpen(GameObject dot, Vector3 offset)
	{
		if (isAnn) 
		{
			AnnBox.transform.position = new Vector3 (dot.transform.position.x + annX + offset.x, dot.transform.position.y + annY + offset.y, -40);

			AnnBox.transform.GetChild(0).GetComponent<Text>().text = 
			"Nature:\t" +
			dot.GetComponent<markAttribs>().annNature +
			"\nDistance:\t" +
				dot.GetComponent<markAttribs>().distanceToOtherPlayer;
			AnnBox.transform.GetChild(1).GetComponent<Text>().text =
			"\"" + dot.GetComponent<markAttribs>().dialogue + "\"";
			
			AnnBox.SetActive (true);
		}
	}


	//When the user is no longer hovering over point, make annotation box invisible
	public void AnnotationClose()
	{
		AnnBox.SetActive(false);
	}


	// Update is called once per frame
	void Update()
	{
		//Change settings depending on selections
		isChk = chk.isOn;
		isDeath = death.isOn;
		isComm = comm.isOn;
		isAnn = ann.isOn;

		testNum = test.value;

		//For calculating how much 'error' will be calculated for the point slider:
		if (errorInput.text == "" || !float.TryParse (errorInput.text, out SLIDER_ERROR)) {
			errorInput.text = "";
			SLIDER_ERROR = 0.2f;
		}else
			SLIDER_ERROR = float.Parse(errorInput.text);

		//For when we swap b/w point and range sliders
		if (rangeSlider.isOn) {
			minTime = sliderObjMin.GetComponent<CustomUiElements.MinimumSlider> ().value;
			maxTime = sliderObjMax.GetComponent<CustomUiElements.MaximumSlider> ().value;
			//distBlocker.enabled = true;
			pointBlocker.enabled = true;
			rangeBlocker.enabled = false;
		} else {
			minTime = singleSliderObj.GetComponent<Slider> ().value;
			maxTime = 5.0f - SLIDER_ERROR - minTime;
			//distBlocker.enabled = false;
			rangeBlocker.enabled = true;
			pointBlocker.enabled = false;
		}

		//Turns off Annotation boxes if players don't want them
		if (!isAnn)
			AnnBox.SetActive (false);


		//If we're going to show all lines at once or not
		if (isDist.isOn)
			forceLine = true;
		else
			forceLine = false;
	}
}
