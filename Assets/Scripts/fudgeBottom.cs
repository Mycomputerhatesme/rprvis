﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fudgeBottom : MonoBehaviour 
{

	public Toggle checkpoint;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		GetComponent<Image>().enabled = checkpoint.isOn;
	}
}
