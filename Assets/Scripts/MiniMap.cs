﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
    public float zoomSize = 1.0f;
    float newZoomSize = 1.0f;
    Vector3 newZoomPosition;
    Vector3 zoomPosition;
    Vector2 minPosition;
    Vector2 maxPosition;
    Vector2 aspectRatio = Vector2.one;
    Vector2 aspectScaler = Vector2.one;

    public ZoomMap zoomMap;

    public List<markAttribs> _allMarks;
    public List<markAttribs> _checkpoints;
    public List<markAttribs> _deaths;
    public List<markAttribs> _comms;

    RectTransform rectTransform;
    Vector2 dimensions;
    Vector2 dimensionsHalf;

    public GameObject mapSelectionWindow;
    RectTransform mapSelectionRect;
    Vector2 mapSelectionAspect;
    bool draggingMinimap = false;
    bool isInit = false;

	//For calculating average lengths of player distances
	public int averageCount;
	public float totalAvg;
	public Text avgDisplay;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(LateStart(0.1f));
    }

    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        //Your Function You Want to Call
        rectTransform = GetComponent<RectTransform>();

        FillList(out _checkpoints, "Checkpoint");
        FillList(out _deaths, "Death");
        FillList(out _comms, "Comm");

        _allMarks = new List<markAttribs>();
        _allMarks.AddRange(_checkpoints);
        _allMarks.AddRange(_deaths);
        _allMarks.AddRange(_comms);

        minPosition = Vector2.zero;
        maxPosition = Vector2.zero;
        foreach (markAttribs mark in _allMarks)
        {
            Vector3 position = mark.transform.localPosition;
            if (position.x < minPosition.x)
                minPosition.x = position.x;
            if (position.y < minPosition.y)
                minPosition.y = position.y;
            if (position.x > maxPosition.x)
                maxPosition.x = position.x;
            if (position.y > maxPosition.y)
                maxPosition.y = position.y;

            //Debug.Log("MinPos " + minPosition + "\nMaxPos " + maxPosition);
            
        }
        
        Vector2 positionDifference = maxPosition - minPosition;
        foreach (markAttribs mark in _allMarks)
        {
            mark.transform.localPosition -= new Vector3(minPosition.x, minPosition.y, 0f);
        }
        dimensions = rectTransform.rect.size;
        dimensionsHalf = dimensions * 0.5f;
        aspectRatio.x = dimensions.x / dimensions.y;

        if (positionDifference.x / aspectRatio.x > positionDifference.y)
        {
            aspectScaler.y = positionDifference.x / aspectRatio.y / positionDifference.y;
        }
        else if (positionDifference.y / aspectRatio.y > positionDifference.x)
        {
            aspectScaler.x = positionDifference.y / aspectRatio.x / positionDifference.x;
        }


        maxPosition.Scale(aspectScaler);
        minPosition.Scale(aspectScaler);

        Vector2 originalOffset = positionDifference;
        positionDifference.Scale(aspectScaler);
        originalOffset = positionDifference - originalOffset;

        foreach (markAttribs mark in _allMarks)
        {
            mark.transform.localPosition.Scale(new Vector3(aspectScaler.x, aspectScaler.y, 1f));
        }

        Vector2 centerPosition = Vector2.Lerp(minPosition, maxPosition, 0.5f);
        centerPosition = positionDifference * 0.5f;
        

        minPosition = Vector2.zero;
        maxPosition = positionDifference;
        minPosition = Vector2.LerpUnclamped(centerPosition, centerPosition - positionDifference * 0.5f, 1.1f);
        maxPosition = Vector2.LerpUnclamped(centerPosition, centerPosition + positionDifference * 0.5f, 1.1f);

        foreach (markAttribs mark in _allMarks)
        {
            Vector3 newPosition = new Vector3(0f, 0f, 0f);
            newPosition.x = originalOffset.x * 0.5f + Mathf.LerpUnclamped(-dimensionsHalf.x, dimensionsHalf.x, Mathf.InverseLerp(minPosition.x, maxPosition.x, mark.transform.localPosition.x));
            newPosition.y = originalOffset.y * 0.5f + Mathf.LerpUnclamped(-dimensionsHalf.y, dimensionsHalf.y, Mathf.InverseLerp(minPosition.y, maxPosition.y, mark.transform.localPosition.y));
            mark.transform.localPosition = newPosition;
        }

        mapSelectionRect = mapSelectionWindow.GetComponent<RectTransform>();
        mapSelectionAspect = new Vector2(mapSelectionRect.rect.height / mapSelectionRect.rect.width, 1.0f);

        zoomPosition = mapSelectionWindow.transform.position;
        newZoomPosition = mapSelectionWindow.transform.position;

        zoomMap = GameObject.Find("ZoomMap").GetComponent<ZoomMap>();
        zoomMap.Init();
        isInit = true;
    }
    

    void FillList(out List<markAttribs> listToFill, string tag)
    {
        listToFill = new List<markAttribs>();
        GameObject[] arrayList = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject go in arrayList)
        {
            listToFill.Add(go.GetComponent<markAttribs>());
        }
    }

    void FixedUpdate()
    {
        if(isInit)
        {
            float size = Mathf.Min(rectTransform.rect.width, rectTransform.rect.height);
            zoomSize = Mathf.Lerp(zoomSize, newZoomSize, 0.2f);
            //zoomPosition = mapSelectionWindow.transform.position;
            //Debug.Log(zoomPosition + " " + newZoomPosition);
            zoomPosition = Vector3.Lerp(zoomPosition, newZoomPosition, 0.2f);
            mapSelectionRect.sizeDelta = new Vector2(size * zoomSize, size * zoomSize);
            mapSelectionRect.sizeDelta.Scale(mapSelectionAspect);

            ClampSelection();
            zoomMap.UpdateMap(mapSelectionRect);
        }
    }

    void ClampSelection()
    {
        Vector3 newPos = mapSelectionRect.position;

        if(newPos.x > rectTransform.position.x + rectTransform.rect.xMax - mapSelectionRect.rect.width * 0.5f)
        {
            newPos.x = rectTransform.rect.x + rectTransform.position.x + rectTransform.rect.width - mapSelectionRect.rect.width * 0.5f;
        }
        if (newPos.y > rectTransform.position.y + rectTransform.rect.yMax - mapSelectionRect.rect.height * 0.5f)
        {
            newPos.y = rectTransform.rect.y + rectTransform.position.y + rectTransform.rect.height - mapSelectionRect.rect.height * 0.5f;
        }
        if (newPos.x < rectTransform.position.x + rectTransform.rect.xMin + mapSelectionRect.rect.width * 0.5f)
        {
            newPos.x = rectTransform.rect.x + rectTransform.position.x + mapSelectionRect.rect.width * 0.5f;
        }
        if (newPos.y < rectTransform.position.y + rectTransform.rect.yMin + mapSelectionRect.rect.height * 0.5f)
        {
            newPos.y = rectTransform.rect.y + rectTransform.position.y + mapSelectionRect.rect.height * 0.5f;
        }
        
        mapSelectionRect.position = newPos;
    }

    // Update is called once per frame
    void Update ()
    {
        if(isInit)
        {
            if(Input.mouseScrollDelta.y > 0.0f)
            {
                newZoomSize /= 1.0f + Input.mouseScrollDelta.y * 0.5f;
            }
            else if (Input.mouseScrollDelta.y < 0.0f)
            {
                newZoomSize *= 1.0f - Input.mouseScrollDelta.y * 0.5f;
            }
    
            Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if(Input.GetMouseButton(0))
            {
                if (rectTransform.rect.Contains(mousePosition - rectTransform.rect.size * 0.5f))
                {
                    draggingMinimap = true;
                }
            }
    
            mapSelectionWindow.transform.position = zoomPosition; 
            ClampSelection();
    
            if(draggingMinimap)
            {
                newZoomPosition = new Vector3(mousePosition.x, mousePosition.y, 0f);
                ClampSelection();
                zoomMap.UpdateMap(mapSelectionRect);
            }
    
            if (Input.GetMouseButtonUp(0))
            {
                draggingMinimap = false;
            }
    
            newZoomSize = Mathf.Clamp(newZoomSize, 0.01f, 0.9f);


			//Calculating/displaying the average distances of pins that are displayed
			totalAvg = 0.0f;
			averageCount = 0;

			for (int i = 0; i < _allMarks.Count; ++i){
				if (_allMarks [i].GetComponent<Image> ().enabled) {
					++averageCount;
					totalAvg += _allMarks [i].distanceToOtherPlayer;
				}
			}
			totalAvg /= averageCount;

			avgDisplay.text = totalAvg.ToString () + " Units";
        }
    }
}
