﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomMap : MonoBehaviour
{
    [SerializeField]
    public Markers[] _iconImages;

    public GameObject _iconPrefab;
    public GameObject _iconChild;
    public MiniMap _miniMap;

    RectTransform mapTransform;

    List<markAttribs> _allMarks;
    List<markAttribs> _checkpoints;
    List<markAttribs> _deaths;
    List<markAttribs> _comms;


    List<GameObject> _allMarkGOs;
    List<GameObject> _checkpointGOs;
    List<GameObject> _deathGOs;
    List<GameObject> _commGOs;


    private visCtrl visMain;

    // Use this for initialization
    public void Init ()
    {
        visMain = GameObject.Find("MainCtrl").GetComponent<visCtrl>();
        mapTransform = GetComponent<RectTransform>();

        _miniMap.zoomMap = this;
        _allMarks = _miniMap._allMarks;
        _checkpoints = _miniMap._checkpoints;
        _deaths = _miniMap._deaths;
        _comms = _miniMap._comms;

        ListToGameObject(out _checkpointGOs, _checkpoints);
        ListToGameObject(out _deathGOs, _deaths);
        ListToGameObject(out _commGOs, _comms);

        _allMarkGOs = new List<GameObject>();
        _allMarkGOs.AddRange(_checkpointGOs);
        _allMarkGOs.AddRange(_deathGOs);
        _allMarkGOs.AddRange(_commGOs);

        for (int i = 0; i < _allMarkGOs.Count; ++i)
        {
            _allMarkGOs[i].GetComponent<Image>().sprite = _allMarks[i].GetComponent<Image>().sprite;
            _allMarkGOs[i].GetComponent<Image>().color = _allMarks[i].GetComponent<Image>().color;
            _allMarkGOs[i].GetComponent<ZoomMapAttribs>()._markAttribs = _allMarks[i];
        }
    }

    void ListToGameObject(out List<GameObject> listToFill, List<markAttribs> listToReference)
    {
        listToFill = new List<GameObject>();
        foreach (markAttribs mark in listToReference)
        {
            listToFill.Add(Instantiate(_iconPrefab, _iconChild.transform));
        }
    }

    public void UpdateMap(RectTransform selectWindow)
    {
        for(int i = 0; i < _allMarkGOs.Count; ++i)
        {
            _allMarkGOs[i].GetComponent<Image>().enabled = _allMarks[i].GetComponent<Image>().enabled;
            Vector3 newPos = Vector3.zero;
            Vector3 newPosOther = Vector3.zero;
            newPos.x = InverseLerpU(selectWindow.rect.xMin + selectWindow.transform.position.x, selectWindow.rect.xMax + selectWindow.transform.position.x, _allMarks[i].transform.position.x);
            newPos.y = InverseLerpU(selectWindow.rect.yMin + selectWindow.transform.position.y, selectWindow.rect.yMax + selectWindow.transform.position.y, _allMarks[i].transform.position.y);
                       
            newPosOther.x = InverseLerpU(selectWindow.rect.xMin + selectWindow.transform.position.x, selectWindow.rect.xMax + selectWindow.transform.position.x, _allMarks[i].OtherPlayer.transform.position.x);
            newPosOther.y = InverseLerpU(selectWindow.rect.yMin + selectWindow.transform.position.y, selectWindow.rect.yMax + selectWindow.transform.position.y, _allMarks[i].OtherPlayer.transform.position.y);
              
            _allMarkGOs[i].GetComponent<ZoomMapAttribs>().inverseLerpResult = newPosOther;

            if(newPos.x >= 0.98f || newPos.x <= 0.02f || newPos.y >= 0.98f || newPos.y <= 0.02f)
            {
                //newPos.z = 300.0f;
                //newPos.x = -10000.0f;
                //_allMarkGOs[i].transform.position
            }
            newPos.x = newPos.x * mapTransform.rect.width - mapTransform.position.x * 0.5f;
            newPos.y = newPos.y * mapTransform.rect.height - mapTransform.position.y;

            newPosOther.x = newPosOther.x * mapTransform.rect.width - mapTransform.position.x * 0.5f;
            newPosOther.y = newPosOther.y * mapTransform.rect.height - mapTransform.position.y;

            _allMarkGOs[i].transform.localPosition = newPos;
            _allMarkGOs[i].GetComponent<ZoomMapAttribs>().otherPlayerLocation = newPosOther;
        }
    }


    float InverseLerp(float v0, float v1, float v) 
    {	
        return Mathf.Clamp01((v - v0) / (v1 - v0));
    }

    float InverseLerpU(float v0, float v1, float v) 
    {	
        return (v - v0) / (v1 - v0);
    }

    Vector3 InverseLerpU(Vector3 v, Vector3 v0, Vector3 v1) 
    {	
        Vector3 interp = Vector3.zero;
        interp.x = (v.x - v0.x) / (v1.x - v0.x);
        interp.y = (v.y - v0.y) / (v1.y - v0.y);
        interp.z = (v.z - v0.z) / (v1.z - v0.z);
        return interp;
    }


    

    // Update is called once per frame
    void Update ()
    {
		//Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //RaycastHit hitInfo;
        //if(Physics.Raycast(ray, out hitInfo)) 
        //{
        //    Debug.Log("Hit");
        //    if(hitInfo.transform.GetComponent<ZoomMapAttribs>() != null)
        //    {
        //        ZoomMapAttribs marker = hitInfo.transform.GetComponent<ZoomMapAttribs>();
        //        visMain.AnnotationOpen(marker._markAttribs.gameObject);
        //    }
        //}
        //else
        //{
        //    Debug.Log("Miss");
        //}
        //
        //if(hitInfo.transform != null)
        //    Debug.Log(hitInfo.transform.gameObject);
	}
}
